import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-alert',
  template: '<div>This is an alert: {{message}}</div>',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  @Input()
  message: string;

  constructor() { }

  ngOnInit() {
  }

}
